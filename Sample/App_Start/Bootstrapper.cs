using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using System.Data.Common;
using Data;
using Data.Repository;
using System.Configuration;
using Data.Query;

namespace Sample1
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();    
            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["SampleConnection"];
            string connectionString = connectionStringSettings.ConnectionString;
            string providerName = connectionStringSettings.ProviderName;


            DbProviderFactory databaseProviderFactory = DbProviderFactories.GetFactory(providerName);
            container.RegisterInstance(databaseProviderFactory);


            container.RegisterType<ISampleDbFactory, SampleDbFactory>(new InjectionConstructor(databaseProviderFactory, connectionString));

            container.RegisterType<IQueryFactory, QueryFactory>();

            container.RegisterType<IStudentRepository, StudentRepository>();

        }
    }
}