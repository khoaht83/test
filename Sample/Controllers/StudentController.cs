﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;
using Data.Criteria;
using Data.Repository;
using Sample1.Models;

namespace Sample1.Controllers
{
    public class StudentController : Controller
    {
        IStudentRepository studentRepository;
        public StudentController(IStudentRepository studentRepository)
        {
            this.studentRepository = studentRepository;
        }
        //
        // GET: /Student/
        public ActionResult Index(StudentCriteria criteria)
        {
            var students = studentRepository.Search(criteria);
            return View(students);
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(Guid Id)
        {
            var udata = studentRepository.GetById(Id);
            return View(udata);
        }

        [HttpPost]
        public ActionResult Edit(Student model)
        {
            var udata = studentRepository.Update(model);
            return RedirectToAction("Index");
        }


        public ActionResult Delete(Guid Id)
        {
            var udata = studentRepository.Delete(Id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(CreateStudentModel model)
        {
            // Will be replaced by AutoMapper later
            var newStudent = new Student()
            {
                Name = model.Name,
                Year = model.Year
            };

            if (ModelState.IsValid)
            {
                var student = studentRepository.Create(newStudent);
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }
    }
}