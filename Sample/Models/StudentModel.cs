﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Sample1.CustomValidations;

namespace Sample1.Models
{
    public class CreateStudentModel
    {
        [Required]
        [UniqueName]
        public string Name { get; set; }
        
        [Range(1, 300, ErrorMessage = "Year must between 1 to 300")]
        public int Year { get; set; }
    }
}