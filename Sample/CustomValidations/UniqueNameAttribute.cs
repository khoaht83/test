﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Data.Repository;

namespace Sample1.CustomValidations
{
    public class UniqueNameAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Basically check the DB for any duplicates of the 'value'. 
            //It would be great if we can generalize this method to use with any field
            var stu = DependencyResolver.Current.GetService(typeof(IStudentRepository)) as IStudentRepository;
            if (stu.CheckExist(value as string))
                return (new ValidationResult("Name can not be duplicated"));
            return ValidationResult.Success;

        }
    }
}
