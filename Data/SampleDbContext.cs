﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class SampleDbContext : DbContext
    {
        public SampleDbContext(string connectionstring)
            : base(connectionstring)
        {
            Database.SetInitializer<SampleDbContext>(null);
        }

        public SampleDbContext(DbConnection existingConnection)
            : base(existingConnection, true)
        {
        }

        public DbSet<Student> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Remove unused conventions
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Student>().ToTable("Students");

        }
    }
}
