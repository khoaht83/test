﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Query
{
    public abstract class QueryBase<TResult> : IQuery<TResult>
    {
        protected readonly SampleDbContext context;
        public QueryBase(SampleDbContext context)
        {
            this.context = context;
        }

        public abstract TResult Execute();
    }
}
