﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IQueryFactory
    {
        IOrderedQuery<Student> CreateStudentList(Expression<Func<Student, bool>> predicate, int start, int max);
        
    }
}
