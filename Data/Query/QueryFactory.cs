﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Query
{
    public class QueryFactory : IQueryFactory
    {
        private readonly ISampleDbFactory dbContextFactory;
        private SampleDbContext dbContext;
        public QueryFactory(ISampleDbFactory dbContextFactory)
        {
            this.dbContextFactory = dbContextFactory;
        }

        private SampleDbContext DbContext
        {
            get
            {
                return dbContext ?? (dbContext = dbContextFactory.Get());
            }
        }


        public IOrderedQuery<Student> CreateStudentList(System.Linq.Expressions.Expression<Func<Student, bool>> predicate, int start, int max)
        {
            return new StudentQuery(DbContext, predicate).Page(start, max);
        }
    }
}
