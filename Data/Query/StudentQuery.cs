﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Data.Query
{
    public class StudentQuery : OrderedQueryBase<Student>
    {

        public StudentQuery(SampleDbContext context, Expression<Func<Student, bool>> predicate)
            : base(context)
        {

            OriginalQuery = context.Students.Where(predicate)
                                .OrderBy(t => t.Name);

            Query = OriginalQuery;
        }

        public override IEnumerable<Student> Execute()
        {
            return Query.ToList();
        }

        public override int Count()
        {
            return OriginalQuery.Count();
        }
    }
}
