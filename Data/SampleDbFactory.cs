﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class SampleDbFactory : IDisposable, ISampleDbFactory
    {

        private readonly DbProviderFactory providerFactory;
        private readonly string connectionString;
        private SampleDbContext dbContext;

        public SampleDbFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public SampleDbFactory(DbProviderFactory providerFactory, string connectionString)
        {
            this.providerFactory = providerFactory;
            this.connectionString = connectionString;
        }


        public SampleDbContext Get()
        {
            if (dbContext == null)
            {
                DbConnection connection = providerFactory.CreateConnection();

                if (connection != null)
                {
                    connection.ConnectionString = connectionString;

                    dbContext = new SampleDbContext(connection);
                }

                return dbContext;
            }

            return dbContext;
        }

        public void Dispose()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}

