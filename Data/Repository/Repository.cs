﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Criteria;

namespace Data.Repository
{
    public abstract class Repository<T, E> : IRepository<T, E>
        where T : class, IEntity
        where E : ICriteriaBase
    {

        protected DbSet<T> DbSet;
        private SampleDbContext dbContext;
        private string ConnectionName = "SampleConnection";
        protected Repository(IQueryFactory queryFactory)
        {
            dbContext = new SampleDbContext(ConnectionName);
            DbSet = dbContext.Set<T>();
            QueryFactory = queryFactory;
        }

        public Repository()
        {
            dbContext = new SampleDbContext(ConnectionName);
            DbSet = dbContext.Set<T>();
        }

        public T Create(T entity)
        {

            var newObject = DbSet.Add(entity);
            newObject.Id = Guid.NewGuid();
            Commit();
            return newObject;
        }

        public int Commit()
        {
            try
            {
                return dbContext.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public T Update(T entity)
        {
            try
            {
                var entry = dbContext.Entry(entity);
                dbContext.Set<T>().Attach(entity);
                entry.State = EntityState.Modified;
                dbContext.SaveChanges();
                return entity;
            }
            catch (OptimisticConcurrencyException ex)
            {
                throw ex;
            }
        }

        public bool Delete(T entity)
        {
            DbSet.Remove(entity);
            return dbContext.SaveChanges() > 0;
        }

        public bool Delete(Guid Id)
        {
            var obj = GetById(Id);
            if (obj != null)
            {
                DbSet.Remove(obj);
            }
            return dbContext.SaveChanges() > 0;
        }

        public ParallelQuery<T> GetAll()
        {
            return DbSet.AsParallel();
        }

        public T GetById(Guid Id)
        {
            return DbSet.SingleOrDefault(t => t.Id.Equals(Id));
        }

        protected IQueryFactory QueryFactory
        {
            get;
            private set;
        }

        public void Dispose()
        {
            if (dbContext != null)
                dbContext = null;
        }

        public virtual IEnumerable<T> Search(E criteria)
        {
            return default(ParallelQuery<T>);
        }
    }
}
