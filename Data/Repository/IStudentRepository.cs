﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Criteria;

namespace Data.Repository
{
    public interface IStudentRepository : IRepository<Student, StudentCriteria>
    {
        bool CheckExist(string name);
    }
}
