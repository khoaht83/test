﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Data.Criteria;

namespace Data.Repository
{
    public class StudentRepository : Repository<Student, StudentCriteria>, IStudentRepository
    {

        private readonly IQueryFactory queryFactory;

        public StudentRepository(IQueryFactory queryFactory)
        {
            this.queryFactory = queryFactory;
        }
        public override IEnumerable<Student> Search(StudentCriteria criteria)
        {
            Expression<Func<Student, bool>> predicate = null;

            predicate = c => string.IsNullOrEmpty(criteria.Name) || c.Name.Contains(criteria.Name);

            var query = queryFactory.CreateStudentList(predicate,0,1000);

            return query.Execute();


        }

        public bool CheckExist(string name)
        {
            var students = GetAll().Where(st => st.Name.Equals(name)).SingleOrDefault();
            return students != null;
        }
    }
}
