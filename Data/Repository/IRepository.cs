﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Criteria;

namespace Data.Repository
{
    public interface IRepository<T, E> : IDisposable
        where T : class,IEntity
        where E : ICriteriaBase
    {
        T Create(T entity);

        T Update(T entity);

        bool Delete(T entity);

        bool Delete(Guid Id);

        ParallelQuery<T> GetAll();

        T GetById(Guid Id);
        IEnumerable<T> Search(E criteria);
    }

    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
