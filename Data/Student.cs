﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Repository;

namespace Data
{
    [Table("Students")]
    public class Student : IEntity
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
    }
}
