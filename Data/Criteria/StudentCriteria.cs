﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Criteria
{
    public class StudentCriteria : ICriteriaBase
    {
        public string Name { get; set; }
    }
}
